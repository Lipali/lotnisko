package client;

import java.awt.*;
import java.util.ArrayList;

public class PlaneRoad {
    ArrayList<Point> road = new ArrayList<>();
    Point point= new Point() ;
    public PlaneRoad(Point point){
        this.road.add(point);
        int x= point.getX();
        int y = point.getY();
        int h = point.getH();
        if(x==0) {
            if (y>500){
                while(y>=501){
                    y--;
                    this.road.add(new Point(x,y,h));
                }
            }
            else if (y<500) {
                while(y<=499){
                    y++;
                    this.road.add(new Point(x,y,h));
                }
            }
            for (int i=0 ; i< (500 -h);i++ ){
                x++;
                this.road.add(new Point(x,y,h));
            }
            while (x<=499){
                x++;
                h--;
                this.road.add(new Point(x,y,h));
            }
        }
        else if (x==1000){
            if (y>500){
                while(y>=501){
                    y--;
                    this.road.add(new Point(x,y,h));
                }
            }
            else {
                while(y<=499){
                    y++;
                    this.road.add(new Point(x,y,h));
                }
            }
            for (int i=0 ; i< (500 -h);i++ ){
                x--;
                this.road.add(new Point(x,y,h));
            }
            while (x>500){
                x--;
                h--;
                this.road.add(new Point(x,y,h));
            }

        }
        else if (y==0) {
            if (x > 500) {
                while (x >= 501){
                    x--;
                    this.road.add(new Point(x,y,h));
                }
            } else if (x<500) {
                while (x <= 499) {
                    x++;
                    this.road.add(new Point(x,y,h));
                }
            }
            for (int i = 0; i < (500 - h); i++) {
                y++;
                this.road.add(new Point(x,y,h));
            }
            while (y <= 499) {
                y++;
                h--;
                this.road.add(new Point(x,y,h));
            }
        }
        else if (y==1000) {
            if (x > 500) {
                while (x >= 501){
                    x--;
                    this.road.add(new Point(x,y,h));
                }

            } else if (x<500) {
                while (x <= 499)
                    x++;
                this.road.add(new Point(x,y,h));
            }
            for (int i = 0; i < (500 - h); i++) {
                y--;
                this.road.add(new Point(x,y,h));
            }
            while (y > 500) {
                y--;
                h--;
                this.road.add(new Point(x,y,h));
            }
        }

    }
    public void setRoad(ArrayList<Point> roadParameter) {
        road = roadParameter;
    }
    public ArrayList<Point> getRoad() {
        return road;
    }
    public int getRoadSize (){
        return road.size();
    }
    public Point getPoint (int i){
        return road.get(i);
    }
}

