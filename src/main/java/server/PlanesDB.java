package server;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.LinkedList;

public class PlanesDB {
    private Connection connect() {

        String url = "jdbc:sqlite:C://sqlite/airport.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    public void insertStart (String name, String coord, long time ) {
        String tempA = "???";
        String sql = "INSERT INTO planes  (name,coord,time,status) VALUES(?,?,?,?)";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.setString(2, coord);
            pstmt.setLong(3, time);
            pstmt.setString(4, tempA);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public void update( String name ,String status) {
        String sql = "UPDATE planes SET status = ? "
                + " WHERE name  = ? " ;

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, status);
            pstmt.setString(2, name );
            // update
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public LinkedList<String> selectNames() {
        LinkedList<String> list= new LinkedList<>();
        String sql = "SELECT  name FROM planes";

        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            while (rs.next()) {

                list.add( rs.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }
    public long selectTime(String name) {
        long time =0;
        String sql = "SELECT time FROM planes WHERE name = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)){
            pstmt.setString(1,name);
            ResultSet rs = pstmt.executeQuery(); {

                while (rs.next())

                    time =rs.getLong("time");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return time;
    }

}



