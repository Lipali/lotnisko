package server;

import java.util.LinkedList;

public class PlaneControler {

    public boolean checkCoord (String coord, LinkedList<String> coords){
        int counter = 0;
        for (String position  : coords){
            for (int i=0 ; i<3 ;i++){
                int smallCounter =0;
                if ( (changer(coord)[i]==changer(position)[i]+1) ||(changer(coord)[i]==(changer(position)[i])-1) ){
                    smallCounter++;
                }
                if (smallCounter==3){
                    counter++;
                }
            }
        }
        if (counter==0)
            return true;
        else
            return false;
    }
    public int[] changer (String coord){
        String [] values = coord.split("-");
        int [] cordNumbers = new int[3];
        for (int i =0;i<values.length;i++){
            cordNumbers[i]=Integer.valueOf(values[i]);
        }
        return cordNumbers;
    }
    boolean checkTime (long timeStart){
        long timeNow =System.currentTimeMillis();
        if (timeNow -10800000>timeStart)
            return false;
        else
            return true;
    }
    boolean checkNameList (String name , LinkedList<String>names){
        boolean flag = false;
        for (String listedName : names){
            if (listedName.equals(name)){
                flag = true;
                break;
            }
        }
        if (flag==true)
            return false;
        else
            return true;
    }
    LinkedList<String> canIFlyListMaker (String coord , LinkedList<String>coords){
        for (int i =0;i<coords.size();i++){
            if (coords.get(i).equals(coord)){
                coords.remove(i);
            }
        }
        return coords;
    }

}
